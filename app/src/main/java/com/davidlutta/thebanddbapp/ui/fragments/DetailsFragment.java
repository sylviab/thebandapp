package com.davidlutta.thebanddbapp.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.davidlutta.thebanddbapp.R;
import com.davidlutta.thebanddbapp.database.BandDatabase;
import com.davidlutta.thebanddbapp.model.Band;

public class DetailsFragment extends Fragment {
    private String mBandName;
    private String mBandDescription;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(String param1, String param2) {
        return new DetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mBandName = args.getString("bandName");
            mBandDescription = args.getString("bandDescription");
        } else {
            mBandName = "Not Received";
            mBandDescription = "Not Received";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        TextView nameTextView = view.findViewById(R.id.bandName);
        nameTextView.setText(mBandName);
        TextView descriptionTextView = view.findViewById(R.id.bandDescription);
        descriptionTextView.setText(mBandDescription);
        return view;
    }
}