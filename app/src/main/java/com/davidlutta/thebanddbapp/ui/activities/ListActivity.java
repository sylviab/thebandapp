package com.davidlutta.thebanddbapp.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.davidlutta.thebanddbapp.R;
import com.davidlutta.thebanddbapp.ui.fragments.ListFragment;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
//        Toast.makeText(this, "Hello World", Toast.LENGTH_SHORT).show();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new ListFragment();
        fragmentManager.beginTransaction().add(R.id.listFragmentContainer, ListFragment.newInstance()).commit();
        /*Fragment fragment = fragmentManager.findFragmentById(R.id.listFragmentContainer);
        if (fragment == null) {
            fragment = new ListFragment();
            fragmentManager.beginTransaction().add(R.id.listFragmentContainer, fragment).commit();
        }*/
    }
}