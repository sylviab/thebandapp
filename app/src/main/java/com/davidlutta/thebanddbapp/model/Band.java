package com.davidlutta.thebanddbapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Band implements Parcelable {
    private int mId;
    private String mName;
    private String mDescription;

    public Band(int mId, String mName, String mDescription) {
        this.mId = mId;
        this.mName = mName;
        this.mDescription = mDescription;
    }

    public Band() {
    }

    protected Band(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
        mDescription = in.readString();
    }

    public static final Creator<Band> CREATOR = new Creator<Band>() {
        @Override
        public Band createFromParcel(Parcel in) {
            return new Band(in);
        }

        @Override
        public Band[] newArray(int size) {
            return new Band[size];
        }
    };

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(mId);
        parcel.writeValue(mName);
        parcel.writeValue(mDescription);
    }

    @Override
    public String toString() {
        return "Band{" +
                "mId=" + mId +
                ", mName='" + mName + '\'' +
                ", mDescription='" + mDescription + '\'' +
                '}';
    }
}
